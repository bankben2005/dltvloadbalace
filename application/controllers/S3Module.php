<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
defined('BASEPATH') OR exit('No direct script access allowed');

class S3Module extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    private $current_rating_data_table;
    private $current_rating_data_daily_devices;

    private $sqlsrv_conn;
    private $conn_mysql;
    private $temporary_conn;


    public function __construct(){
        parent::__construct();
        //echo 'aaa';exit;
        $this->connectTemporaryDB();
        $this->setCurrentDataTable();
        $this->createMonthYearRatingDataTable();
        $this->createTemporaryMonthYearRatingDataTable();
        $this->createTemporaryRatingDataDailyDevices();
        $this->connectS3WebserverMYSQLRating();

        
    }

    public function index(){

        $userip = $this->getUserIP();
        if($this->input->post('request') || file_get_contents('php://input')){

            $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->input->post('request');

            // $log_file_path = $this->createLogFilePath('RequestS3Rating');
            //     $file_content = date("Y-m-d H:i:s") . ' post value : ' . $request . "\n";
            //     file_put_contents($log_file_path, $file_content, FILE_APPEND);
            //     unset($file_content);

                //echo $request;exit;
            $ip_address = $this->getUserIP();
                //$ip_address = '14.207.41.209';

            /* make pretty string */
            $rating_string = $request;
            $rating_string = str_replace('START:', '', $rating_string);
            $rating_string = str_replace('END', '', $rating_string);


                //echo $rating_string;exit;
            /* remove bracket on first character and last character */
            $rating_string = substr($rating_string, 1);
            $rating_string = substr($rating_string, 0,-1);

                //echo $rating_string;exit;

            $exRatingString = explode(';', $rating_string);
            $arrRating = array();
            $arrRating = array_filter($exRatingString, function($value) { return $value !== ''; });


                // print_r($arrRating);exit;

            $awesomeArray = array();
            $awesomeArray['records'] = array();
            $awesomeArray['ip_address'] = $ip_address;

            $ship_code = "";


            /* check version of rating sent  */
            $check_version = $this->checkVersionRatingString($arrRating);
            /* eof check version of rating sent */

                // print_r($check_version);exit;

            $this->setRatingTemporary([
                'awesomeArray'=>$awesomeArray,
                'arrRating'=>$arrRating,
                'ip_address'=>$ip_address
            ]);



            switch ($check_version){
                case 'old_version':
                        # code...
                $this->setRatingOlderVersion([
                    'awesomeArray'=>$awesomeArray,
                    'arrRating'=>$arrRating,
                    'ip_address'=>$ip_address
                ]);
                break;

                case 'new_version':
                $this->setRatingNewVersion([
                    'awesomeArray'=>$awesomeArray,
                    'arrRating'=>$arrRating,
                    'ip_address'=>$ip_address
                ]);

                break;

                default:
                $this->setRatingOlderVersion([
                    'awesomeArray'=>$awesomeArray,
                    'arrRating'=>$arrRating,
                    'ip_address'=>$ip_address
                ]);


                break;
            }



                //print_r($awesomeArray);exit;

                 //    $response = array(
                 //     'status'=>true,
                 //     'request'=>$request,
                 //     'userip'=>$userip
                 //    );

                

                 //    echo json_encode($response);

        }else{
            $response = array(
                'status'=>true,
                'result_code'=>'error',
                'result_desc'=>'Not found request',
                'userip'=>$userip
            );

                    // $log_file_path = $this->createLogFilePath('RequestS3Rating');
                    // $file_content = date("Y-m-d H:i:s") . ' post value : ' . json_encode($response) . "\n";
                    // file_put_contents($log_file_path, $file_content, FILE_APPEND);
                    // unset($file_content);

                    // echo json_encode($response);

        }
    }

    public function createMonthYearRatingDataTable(){
        //echo 'aaaa';
        $query = $this->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$this->current_rating_data_table."'");

        if($query->num_rows() <= 0){
            /* create table for rating data */
            $strQuery = "CREATE TABLE ".$this->current_rating_data_table." (
            id int IDENTITY(1,1) PRIMARY KEY,
            devices_id int,
            channels_id int,
            ship_code varchar(255),
            frq varchar(10),
            sym varchar(10),
            pol varchar(1),
            server_id varchar(10),
            vdo_pid varchar(10),
            ado_pid varchar(10),
            view_seconds int,
            ip_address varchar(50),
            startview_datetime datetime,
            channel_ascii_code int,
            created datetime
        )";

        $this->db->query($strQuery);

        }
    }

    private function createTemporaryMonthYearRatingDataTable(){
        $queryCheck = "select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = '".$this->current_rating_data_table."'";

        $stmt = sqlsrv_query( $this->temporary_conn, $queryCheck,array(),array( "Scrollable" => 'static' ));
        // echo ENVIRONMENT;exit;
        if(sqlsrv_num_rows($stmt) <= 0){

            $strQuery = "CREATE TABLE ".$this->current_rating_data_table." (
            id int IDENTITY(1,1) PRIMARY KEY,
            devices_id int,
            channels_id int,
            ship_code varchar(255),
            frq varchar(10),
            sym varchar(10),
            pol varchar(1),
            server_id varchar(10),
            vdo_pid varchar(10),
            ado_pid varchar(10),
            view_seconds int,
            ip_address varchar(50),
            startview_datetime datetime,
            channel_ascii_code int,
            created datetime
            )";

            sqlsrv_query($this->temporary_conn,$strQuery,array(),array(
                "Scrollable"=>'static'
            ));

        }


    }

    private function createTemporaryRatingDataDailyDevices(){
        $queryCheck = "select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = '".$this->current_rating_data_daily_devices."'";

        $stmt = sqlsrv_query( $this->temporary_conn, $queryCheck,array(),array( "Scrollable" => 'static' ));
        // echo ENVIRONMENT;exit;
        if(sqlsrv_num_rows($stmt) <= 0){

            $strQuery = "CREATE TABLE ".$this->current_rating_data_daily_devices." (
            id int IDENTITY(1,1) PRIMARY KEY,
            rating_data_daily_id int,
            devices_id int,
            ship_code varchar(255),
            total_seconds int,            
            created datetime,
            updated datetime
            )";

            sqlsrv_query($this->temporary_conn,$strQuery,array(),array(
                "Scrollable"=>'static'
            ));

        }
    }





public function UpdateDeviceAddress(){
    if($this->input->post(NULL,FALSE)){

        $devices = $this->checkCreateDeviceRecordByAPI(array(
            'ship_code'=>$this->input->post('chip_code'),
            'ip_address'=>$this->input->post('ip_address'),
            'latitude'=>$this->input->post('latitude'),
            'longitude'=>$this->input->post('longitude')
        ));

        echo json_encode(array(
            'status'=>true
        ));

    }
}

public function testapikey(){

    $data = array(
        'ip_address'=>'1.1.1.1'
    );

    $this->getAddressByIpAddress($data);

}

private function createLogFilePath($filename = '') {
    $log_path = './logs/requestrating';

    $dirs = explode('/', $log_path);

    $checked_log_Path = '';
    $pieces = array();

    foreach ($dirs as $dir) {

        if (trim($dir) != '') {
            $pieces[] = $dir;

            $checked_log_Path = implode('/', $pieces);

            if (!is_dir($checked_log_Path)) {
                mkdir($checked_log_Path);
                chmod($checked_log_Path, 0777);
            }
        }
    }

    $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

    return $log_file_path;
}

private function getUserIP(){
 $client  = @$_SERVER['HTTP_CLIENT_IP'];
 $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
 $remote  = $_SERVER['REMOTE_ADDR'];

 if(filter_var($client, FILTER_VALIDATE_IP))
 {
    $ip = $client;
}
elseif(filter_var($forward, FILTER_VALIDATE_IP))
{
    $ip = $forward;
}
else
{
    $ip = $remote;
}

return $ip;
}

private function getDataValue($data_value){
    $exDatavalue = explode(',',$data_value);
    return array(
        'frq'=>$exDatavalue[0],
        'sym'=>$exDatavalue[1],
        'pol'=>$exDatavalue[2],
        'server_id'=>$exDatavalue[3],
        'vdo_pid'=>$exDatavalue[4],
        'ado_pid'=>$exDatavalue[5],
        'view_seconds'=>$exDatavalue[7],
        'created'=>$this->getRatingDateFormat($exDatavalue[6])
    );

        //print_r($exDatavalue);exit;
}

private function getRatingDateFormat($date_format){
        // $date_format = str_replace('\', '', $date_format);
    $date_format = stripslashes($date_format);
    $date_format = str_replace('-', ' ', $date_format);
        //print_r($date_format);exit;
    $datetime_return = new DateTime($date_format);
        // print_r($datetime_return);exit;
    return $datetime_return->format('Y-m-d H:i:s');
}

private function checkCreateDeviceRecord($arrData){ 


        // temporary for seperate CPU
    return $this->processCheckCreateDeviceRecordV2($arrData);



    $queryCheck = $this->db->select('id,ship_code')
    ->from('devices')
    ->where('ship_code',$arrData['ship_code'])
    ->get();

    if($queryCheck->num_rows() <= 0){

        $this->db->insert('devices',array(
            'ship_code'=>$arrData['ship_code'],
            'ip_address'=>$arrData['ip_address'],
            'created'=>date('Y-m-d H:i:s')
        ));
        $insert_id = $this->db->insert_id();

        if($insert_id){
            $this->load->config('api');

            $arr_province_region = $this->config->item('get_province_region_by_region_name');

            $province_region = "";

            $address = $this->getAddressByIpAddress(array(
                'ip_address'=>$arrData['ip_address']
            ));

            if($address->region_name){
                $key = array_search($address->region_name, array_column($arr_province_region, 'region_name'));

                if(is_numeric($key)){
                    $province_region = $arr_province_region[$key]['province_region'];
                }
            }

            $this->db->insert('device_addresses',array(
                'devices_id'=>$insert_id,
                'continent_code'=>$address->continent_code,
                'continent_name'=>$address->continent_name,
                'country_code'=>$address->country_code,
                'country_name'=>$address->country_name,
                'region_code'=>$address->region_code,
                'region_name'=>$address->region_name,
                'province_region'=>$province_region,
                'city'=>$address->city,
                'zip'=>$address->zip,
                'latitude'=>$address->latitude,
                'longitude'=>$address->longitude,
                'location'=>json_encode($address->location),
                'created'=>date('Y-m-d H:i:s')
            ));
        }

        return $insert_id;


    }else{
        return $queryCheck->row()->id;
    }

}

private function processCheckCreateDeviceRecordV2($arrData){
    $check_created = $this->checkIsCreateDeviceRecordMYSQL($arrData); 

        //print_r($check_created);exit;

    if(!$check_created['status']){ 


            // check already create in SQL SERVER
        $querySql = $this->db->select('id,ship_code')
        ->from('devices')
        ->where('ship_code',$arrData['ship_code'])
        ->get(); 

        $insert_id = 0;
        $in_sqlsrv_status = false;

        if($querySql->num_rows() <= 0){
            $this->db->insert('devices',array(
                'ship_code'=>$arrData['ship_code'],
                'ip_address'=>$arrData['ip_address'],
                'created'=>date('Y-m-d H:i:s')
            ));
            $insert_id = $this->db->insert_id(); 
        }else{
            $insert_id = $querySql->row()->id;
            $in_sqlsrv_status = true;
        }



            // also insert into devices mysql rating 
        $this->insertDeviceToMYSLQ([
            'chipcode'=>$arrData['ship_code'],
            'ref_id'=>$insert_id
        ]); 

        if($insert_id && !$in_sqlsrv_status){
            $this->load->config('api');

            $arr_province_region = $this->config->item('get_province_region_by_region_name');

            $province_region = "";

            $address = $this->getAddressByIpAddress(array(
                'ip_address'=>$arrData['ip_address']
            ));

            if($address->region_name){
                $key = array_search($address->region_name, array_column($arr_province_region, 'region_name'));

                if(is_numeric($key)){
                    $province_region = $arr_province_region[$key]['province_region'];
                }
            }

            $this->db->insert('device_addresses',array(
                'devices_id'=>$insert_id,
                'continent_code'=>$address->continent_code,
                'continent_name'=>$address->continent_name,
                'country_code'=>$address->country_code,
                'country_name'=>$address->country_name,
                'region_code'=>$address->region_code,
                'region_name'=>$address->region_name,
                'province_region'=>$province_region,
                'city'=>$address->city,
                'zip'=>$address->zip,
                'latitude'=>$address->latitude,
                'longitude'=>$address->longitude,
                'location'=>json_encode($address->location),
                'created'=>date('Y-m-d H:i:s')
            ));




        } 

        return $insert_id;
    }else{
        return $check_created['data']['ref_id'];
    }
}

private function checkIsCreateDeviceRecordMYSQL($arrData){

    $queryCheck = "select ref_id,chipcode from devices where chipcode = '".$arrData['ship_code']."'"; 

    $result = $this->conn_mysql->query($queryCheck); 

    if($result->num_rows <= 0){ 
        return [
            'status'=>false
        ];
    }else{
        $row = $result->fetch_assoc();
        return [
            'status'=>true,
            'data'=>$row
        ];
    }


}
private function checkCreateDeviceRecordByAPI($arrData){

    $queryCheck = $this->db->select('id,ship_code')
    ->from('devices')
    ->where('ship_code',$arrData['ship_code'])
    ->get();
        //echo $this->db->last_query();
        //print_r($queryCheck->row());
    if($queryCheck->num_rows() <= 0){
        /* create new ship code and fill the devices address table */

        $this->db->insert('devices',array(
            'ship_code'=>$arrData['ship_code'],
            'ip_address'=>$arrData['ip_address'],
            'created'=>date('Y-m-d H:i:s')
        ));
            //exit;

        $insert_id = $this->db->insert_id();


        if($insert_id){
            /* insert address for this device */
            $address = $this->getAddressByIpAddress(array(
                'ip_address'=>$arrData['ip_address']
            ));

                //$region_data = $this->filterRegionNameAndGetProvinceRegion($address->region_name);

            $province_region_name = $this->getRegionNameByZipcode(array(
                'zipcode'=>$address->zip
            ));

            $this->db->insert('device_addresses',array(
                'devices_id'=>$insert_id,
                'continent_code'=>$address->continent_code,
                'continent_name'=>$address->continent_name,
                'country_code'=>$address->country_code,
                'country_name'=>$address->country_name,
                'region_code'=>$address->region_code,
                'region_name'=>$address->region_name,
                'city'=>$address->city,
                'zip'=>$address->zip,
                'latitude'=>$arrData['latitude'],
                'longitude'=>$arrData['longitude'],
                'location'=>json_encode($address->location),
                'province_region'=>$province_region_name,
                'created'=>date('Y-m-d H:i:s'),
                'update_from_api'=>1
            ));
        }
            //echo $insert_id;
        return $insert_id;

            //print_r($address);
    }else{
            //return $queryCheck->row()->id;
        /* update device and device addresses */
        $devices_id = $queryCheck->row()->id;

        $this->db->update('devices',array(
            'ip_address'=>$arrData['ip_address'],
            'updated'=>date('Y-m-d H:i:s')
        ),array('id'=>$devices_id));


        $address = $this->getAddressByIpAddress(array(
            'ip_address'=>$arrData['ip_address']
        ));

            // $region_data = $this->filterRegionNameAndGetProvinceRegion($address->region_name);

        $province_region_name = $this->getRegionNameByZipcode(array(
            'zipcode'=>$address->zip
        ));
        $this->db->update('device_addresses',array(
            'continent_code'=>$address->continent_code,
            'continent_name'=>$address->continent_name,
            'country_code'=>$address->country_code,
            'country_name'=>$address->country_name,
            'region_code'=>$address->region_code,
            'region_name'=>$address->region_name,
            'city'=>$address->city,
            'zip'=>$address->zip,
            'latitude'=>$arrData['latitude'],
            'longitude'=>$arrData['longitude'],
            'location'=>json_encode($address->location),
            'province_region'=>$province_region_name,
            'updated'=>date('Y-m-d H:i:s'),
            'update_from_api'=>1
        ),array('devices_id'=>$devices_id));

        /* eof update device*/
    }
    return true;

}

private function getAddressByIpAddress($arrData){

    /* older version */
        // $this->load->config('api');
        // $ipstack_config = $this->config->item('ipstack');
        // $k = array_rand($ipstack_config['api_key']);
        // $api_key = $ipstack_config['api_key'][$k];

        //print_r($api_key);exit;

    $queryAPI = $this->db->select('*')
    ->from('ipstack_api')
    ->where('over_limit_status',0)
    ->order_by('NEWID()')
    ->limit(1)
    ->get();

    $row = $queryAPI->row();
    $api_key = $row->api_key;

        //print_r($api_key);exit;


    $curl = curl_init();
        // Set some options - we are passing in a useragent too here
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'http://api.ipstack.com/'.$arrData['ip_address'].'?access_key='.$api_key.'&format=1',
        CURLOPT_TIMEOUT=>10000
    ));
        // Send the request & save response to $resp
    $resp = curl_exec($curl);

        //echo $resp;
        // Close request to clear up some resources
    curl_close($curl);



        // $json_address = file_get_contents('http://api.ipstack.com/'.$arrData['ip_address'].'?access_key='.$api_key.'&format=1');


    return json_decode($resp);
}

private function getChannelsId($arrData){
    $frq = (int)$arrData['frq'];
    $sym = (int)$arrData['sym'];
    $vdo_pid = (int)$arrData['vdo_pid'];
    $ado_pid = (int)$arrData['ado_pid'];
    $service_id = (int)$arrData['server_id'];

        //echo $frq."<br>".$sym."<br>".$vdo_pid."<br>".$ado_pid;exit;

        // $query = $this->db->select('id,frq,sym,vdo_pid,ado_pid,service_id')
        // ->from('channels')
        // ->where('frq',$frq)
        // ->where('sym',$sym)
        // ->where('vdo_pid',$vdo_pid)
        // ->where('ado_pid',$ado_pid)->get();

    $query = $this->db->select('channel_components.id as channel_components_id,channel_components.channels_id,channel_components.frq,channel_components.sym,channel_components.pol,channel_components.vdo_pid,channel_components.ado_pid,channel_components.service_id as component_service_id')
    ->from('channel_components')
    ->join('channels','channel_components.channels_id = channels.id')
    ->where('channel_components.frq',$frq)
    ->where('channel_components.sym',$sym)
    ->where('channel_components.vdo_pid',$vdo_pid)
    ->where('channel_components.ado_pid',$ado_pid)
    ->where('channels.active',1)
    ->order_by('channel_components.created','desc')
    ->get();


    if(isset($_GET['test']) && $_GET['test'] == 'test'){
        echo $this->db->last_query();exit;
    }


    if($query->num_rows() > 0){
        $row = $query->row();
        if($row->component_service_id == NULL || $row->component_service_id == ''){

            /* update service id into channel_components table */


                // $this->db->update('channel_components',array(
                //     'updated_serviceid_by'=>'api',
                //     'service_id'=>(int)$arrData['server_id'],
                //     'updated'=>date('Y-m-d H:i:s')
                // ),array('id'=>$row->channel_components_id));


            /* eof update service id into channel_components table */

            $this->db->update('channels',array(
                'service_id'=>(int)$arrData['server_id'],
                'updated'=>date('Y-m-d H:i:s')
            ),array('id'=>$row->channels_id));
        }

        return $row->channels_id;
    }else{

        $queryCheckUpdateLogs = $this->db->select('*')
        ->from('channel_components_update_logs')
        ->join('channels','channel_components_update_logs.channels_id = channels.id')
        ->where('channel_components_update_logs.frq',$frq)
        ->where('channel_components_update_logs.sym',$sym)
        ->where('channel_components_update_logs.vdo_pid',$vdo_pid)
        ->where('channel_components_update_logs.ado_pid',$ado_pid)
        ->order_by('channel_components_update_logs.created','desc')
        ->get();
        if($queryCheckUpdateLogs->num_rows() > 0){
            $row = $queryCheckUpdateLogs->row();

            if($row->service_id == NULL || $row->service_id == ''){

                /* update service id into channel_components table */


                    // $this->db->update('channel_components',array(
                    //     'updated_serviceid_by'=>'api',
                    //     'service_id'=>(int)$arrData['server_id'],
                    //     'updated'=>date('Y-m-d H:i:s')
                    // ),array('id'=>$row->channel_components_id));



                /* eof update service id into channel_components table */

                $this->db->update('channels',array(
                    'service_id'=>(int)$arrData['server_id'],
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$row->channels_id));
            }

            return $row->channels_id;

        }else{
            return 0;
        }

    }

}

private function getTemporaryChannelsId($arrData){

    $frq = (int)$arrData['frq'];
    $sym = (int)$arrData['sym'];
    $vdo_pid = (int)$arrData['vdo_pid'];
    $ado_pid = (int)$arrData['ado_pid'];
    $service_id = (int)$arrData['server_id'];

    $query = "select channel_components.id as channel_components_id,channel_components.channels_id,channel_components.frq,channel_components.sym,channel_components.pol,channel_components.vdo_pid,channel_components.ado_pid,channel_components.service_id as component_service_id";

    $query .= " from channel_components";
    $query .= " join channels on channel_components.channels_id = channels.id";
    $query .= " where channel_components.frq = '".$frq."'";
    $query .= " and channel_components.sym = '".$sym."'";
    $query .= " and channel_components.vdo_pid = '".$vdo_pid."'";
    $query .= " and channel_components.ado_pid = '".$ado_pid."'";
    $query .= " and channels.active = 1";
    $query .= " order by channel_components.created desc";

    $stmt = sqlsrv_query( $this->temporary_conn, $query,array(),array( "Scrollable" => 'static' ));

    if(sqlsrv_num_rows($stmt) > 0){

         $row = sqlsrv_fetch_object($stmt);
         if($row->component_service_id == NULL || $row->component_service_id == ''){

            $update_query = "update channels set service_id = '".(int)$arrData['service_id']."',updated = '".date('Y-m-d H:i:s')."' where id = '".$row->channels_id."'";

            sqlsrv_query($this->temporary_conn,$update_query,array(),array('Scrollable'=>'static'));
        }

        return $row->channels_id;

    }else{ 


        $query_update_logs = "select * from channel_components_update_logs";
        $query_update_logs .= " join channels on channel_components_update_logs.channels_id = channels.id";
        $query_update_logs .= " where channel_components_update_logs.frq = '".$frq."'";
        $query_update_logs .= " and channel_components_update_logs.sym = '".$sym."'";
        $query_update_logs .= " and channel_components_update_logs.vdo_pid = '".$vdo_pid."'";
        $query_update_logs .= " and channel_components_update_logs.ado_pid = '".$ado_pid."'";
        $query_update_logs .= " order by channel_components_update_logs.created desc";

        $stmt2 = sqlsrv_query( $this->temporary_conn, $query_update_logs,array(),array( "Scrollable" => 'static' ));

        if(sqlsrv_num_rows($stmt2) > 0){
            $row2 = sqlsrv_fetch_object($stmt2);

            if($row->service_id == NULL || $row->service_id == ''){

                $update_query = "update channels set service_id = '".(int)$arrData['service_id']."',updated = '".date('Y-m-d H:i:s')."' where id = '".$row->channels_id."'"; 

                sqlsrv_query($this->temporary_conn,$update_query,array(),array('Scrollable'=>'static'));


            }

            return $row2->channels_id;



        }else{
            return 0;
        }




    }
    



}


private function setRatingDataDaily($data = array()){

    /* check already record by date */
    $check_record = $this->db->select('*')
    ->from('rating_data_daily')
    ->where('date',$data['date'])
    ->where('channels_id',$data['channels_id'])
    ->get();

    if($check_record->num_rows() <= 0){
        /* insert new record into rating_data_daily */
        $this->db->insert('rating_data_daily',array(
            'date'=>$data['date'],
            'channels_id'=>$data['channels_id'],
            'sum_seconds'=>$data['view_seconds'],
            'reach_devices'=>1,
            'created'=>date('Y-m-d H:i:s')
        ));
        /* eof insert new record*/

        $insert_id = $this->db->insert_id();
        /* insert into rating data daily devices */
        $this->db->insert($this->current_rating_data_daily_devices,array(
            'rating_data_daily_id'=>$insert_id,
            'devices_id'=>$data['devices_id'],
            'ship_code'=>$data['ship_code'],
            'total_seconds'=>$data['view_seconds'],
            'created'=>date('Y-m-d H:i:s')
        ));
        /* eof insert into rating data daily devices */
    }else{
        /* if exist record update into record sum view seconds and check device id */

        $row_data_daily = $check_record->row();


            // $device_chipcode = $this->getDeviceChipCodeById($data['devices_id']);


        /* check and insert device */
        $queryCheckDevice = $this->db->select('id','devices_id','rating_data_daily_id,chip_code,created')
        ->from($this->current_rating_data_daily_devices)
        ->where('devices_id',$data['devices_id'])
        ->where('ship_code',$data['ship_code'])
        ->where('CONVERT(char(10), created,126) >= ',date('Y-m-d'))
        ->where('rating_data_daily_id',$row_data_daily->id)
        ->get();

            // echo $this->db->last_query();exit;

        if($queryCheckDevice->num_rows() <= 0){
            /* insert new and update reach devices for rating data daily */
            $this->db->insert($this->current_rating_data_daily_devices,array(
                'rating_data_daily_id'=>$row_data_daily->id,
                'devices_id'=>$data['devices_id'],
                'ship_code'=>$data['ship_code'],
                'total_seconds'=>$data['view_seconds'],
                'created'=>date('Y-m-d H:i:s'),
                'updated'=>date('Y-m-d H:i:s')
            ));


            /* update rating data daily */
            $this->db->update('rating_data_daily',array(
                'sum_seconds'=>($row_data_daily->sum_seconds + $data['view_seconds']),
                'reach_devices'=>($row_data_daily->reach_devices + 1),
                'updated'=>date('Y-m-d H:i:s')
            ),array('id'=>$row_data_daily->id));
            /* eof update rating data*/
        }else{

            /* update rating data daily device update */
            $this->db->update($this->current_rating_data_daily_devices,array(
                'updated'=>date('Y-m-d H:i:s')
            ),array('id'=>$queryCheckDevice->row()->id));


            /* only sum  view seconds */
            $this->db->update('rating_data_daily',array(
                'sum_seconds'=>($row_data_daily->sum_seconds + $data['view_seconds']),
                'updated'=>date('Y-m-d H:i:s')
            ),array('id'=>$row_data_daily->id));

        }




    }


    /* eof check already record by date*/
}

private function setTemporaryRatingDataDaily($data = array()){

    // $check_record = $this->db->select('*')
    // ->from('rating_data_daily')
    // ->where('date',$data['date'])
    // ->where('channels_id',$data['channels_id'])
    // ->get();
    $query_rating_data_daily = "select * from rating_data_daily where date = '".$data['date']."' and channels_id = '".$data['channels_id']."'";

    $stmt = sqlsrv_query( $this->temporary_conn, $query_rating_data_daily,array(),array( "Scrollable" => 'static' ));
        // echo ENVIRONMENT;exit;

    // print_r($data);exit;
    if(sqlsrv_num_rows($stmt) <= 0){

        // $this->db->insert('rating_data_daily',array(
        //     'date'=>$data['date'],
        //     'channels_id'=>$data['channels_id'],
        //     'sum_seconds'=>$data['view_seconds'],
        //     'reach_devices'=>1,
        //     'created'=>date('Y-m-d H:i:s')
        // ));
        /* eof insert new record*/

        $queryInsertDataDaily = "insert into rating_data_daily(
            date,channels_id,sum_seconds,reach_devices,created
        )values(
            '".$data['date']."',
            '".$data['channels_id']."',
            '".$data['view_seconds']."',
            1,
            '".date('Y-m-d H:i:s')."'
        );  SELECT SCOPE_IDENTITY()"; 

        $resource= sqlsrv_query( $this->temporary_conn, $queryInsertDataDaily,array(),array( "Scrollable" => 'static' ));
        sqlsrv_next_result($resource); 
        sqlsrv_fetch($resource);

        $insert_id = sqlsrv_get_field($resource, 0);

        // echo $insert_id;exit;


        $queryInsertDataDailyDevices = "insert into ".$this->current_rating_data_daily_devices."(
            rating_data_daily_id,devices_id,ship_code,total_seconds,created
        )values(
            '".$insert_id."',
            '".$data['devices_id']."',
            '".$data['ship_code']."',
            '".$data['view_seconds']."',
            '".date('Y-m-d H:i:s')."'
        )";


        sqlsrv_query($this->temporary_conn,$queryInsertDataDailyDevices,array(),array("Scrollable"=>'static'));



    }else{ 

        $row_data_daily = sqlsrv_fetch_object($stmt); 

        $queryCheckDevice = "select id,devices_id,rating_data_daily_id,chip_code,created";

        $queryCheckDevice .= " from ".$this->current_rating_data_daily_devices;
        $queryCheckDevice .= " where devices_id = '".$data['devices_id']."'";
        $queryCheckDevice .= " and ship_code = '".$data['ship_code']."'";
        $queryCheckDevice .= " and CONVERT(char(10), created,126) >= '".date('Y-m-d')."'";
        $queryCheckDevice .= " and rating_data_daily_id = '".$row_data_daily->id."'";

        // echo $queryCheckDevice;exit;


        $stmt_checkdevice = sqlsrv_query( $this->temporary_conn, $queryCheckDevice,array(),array( "Scrollable" => 'static' )); 

        if(sqlsrv_num_rows($stmt_checkdevice) <= 0){

            $queryInsertDataDailyDevices = "insert into ".$this->current_rating_data_daily_devices."(
                rating_data_daily_id,devices_id,ship_code,total_seconds,created,updated
            )values(
                '".$row_data_daily->id."',
                '".$data['devices_id']."',
                '".$data['ship_code']."',
                '".$data['view_seconds']."',
                '".date('Y-m-d H:i:s')."',
                '".date('Y-m-d H:i:s')."'
            )";

            sqlsrv_query($this->temporary_conn,$queryInsertDataDailyDevices,array(),array(
                "Scrollable"=>'static'
            ));



            // $this->db->insert($this->current_rating_data_daily_devices,array(
            //     'rating_data_daily_id'=>$row_data_daily->id,
            //     'devices_id'=>$data['devices_id'],
            //     'ship_code'=>$data['ship_code'],
            //     'total_seconds'=>$data['view_seconds'],
            //     'created'=>date('Y-m-d H:i:s'),
            //     'updated'=>date('Y-m-d H:i:s')
            // ));
            $queryUpdateRatingDataDaily = "update rating_data_daily set sum_seconds = '".($row_data_daily->sum_seconds + $data['view_seconds'])."',reach_devices = '".($row_data_daily->reach_devices+1)."',updated = '".date('Y-m-d H:i:s')."' where id = '".$row_data_daily->id."'";



            sqlsrv_query($this->temporary_conn,$queryUpdateRatingDataDaily,array(),array(
                "Scrollable"=>'static'
            )); 


            /* update rating data daily */
            // $this->db->update('rating_data_daily',array(
            //     'sum_seconds'=>($row_data_daily->sum_seconds + $data['view_seconds']),
            //     'reach_devices'=>($row_data_daily->reach_devices + 1),
            //     'updated'=>date('Y-m-d H:i:s')
            // ),array('id'=>$row_data_daily->id));
            /* eof update rating data*/



        }else{ 

            $row_check_devices = sqlsrv_fetch_object($stmt_checkdevice);


            $update_q = "update ".$this->current_rating_data_daily_devices." set updated = '".date('Y-m-d H:i:s')."' where id = '".$row_check_devices->id."'";

            sqlsrv_query($this->temporary_conn,$update_q,array(),array(
                'Scrollable'=>'static'
            ));



            /* update rating data daily device update */
            // $this->db->update($this->current_rating_data_daily_devices,array(
            //     'updated'=>date('Y-m-d H:i:s')
            // ),array('id'=>$queryCheckDevice->row()->id));


            $update_r_data_daily = "update rating_data_daily set sum_seconds = '".($row_data_daily->sum_seconds + $data['view_seconds'])."',updated = '".date('Y-m-d H:i:s')."' where id = '".$row_data_daily->id."'";

            sqlsrv_query($this->temporary_conn,$update_r_data_daily,array(),array(
                'Scrollable'=>'static'
            ));



            /* only sum  view seconds */
            // $this->db->update('rating_data_daily',array(
            //     'sum_seconds'=>($row_data_daily->sum_seconds + $data['view_seconds']),
            //     'updated'=>date('Y-m-d H:i:s')
            // ),array('id'=>$row_data_daily->id));




        }









        /* check and insert device */
        // $queryCheckDevice = $this->db->select('id','devices_id','rating_data_daily_id,chip_code,created')
        // ->from($this->current_rating_data_daily_devices)
        // ->where('devices_id',$data['devices_id'])
        // ->where('ship_code',$data['ship_code'])
        // ->where('CONVERT(char(10), created,126) >= ',date('Y-m-d'))
        // ->where('rating_data_daily_id',$row_data_daily->id)
        // ->get();

        // echo $this->db->last_query();exit;





    }




}

public function testFilterRegionName(){
    $region_data = $this->filterRegionNameAndGetProvinceRegion('Bangkok');
    print_r($region_data);exit;
}

public function testRegionNameByZipcode(){
    $zipcode = '86130';

    $province_region = $this->getRegionNameByZipcode(array(
        'zipcode'=>$zipcode
    ));

    print_r($province_region);exit;
}
private function filterRegionNameAndGetProvinceRegion($region_name){

    $return_region = $region_name;
        //echo $return_region;
    if (strpos($return_region, 'Changwat ') !== false) {
                //echo 'true';
        $return_region = str_replace('Changwat ', '', $return_region);
    }

    if(strpos($return_region, ' Province') !== false){
        $return_region = str_replace(' Province', '', $return_region);
    }

    $return_region = str_replace(' ', '', $return_region);

    $return_region = strtolower($return_region);

    $return_region = ucfirst($return_region);

        //echo $return_region;exit;
    /* check to province to get province region */

    $query = $this->db->select('*')
    ->from('provinces')
    ->where('province_en_name',$return_region)
    ->get();

    if($query->num_rows() > 0){
        return array(
            'region_name'=>$return_region,
            'province_region'=>$query->row()->province_region
        );
    }else{
        return array(
            'region_name'=>$return_region,
            'province_region'=>''
        );
    }
}


private function setCurrentDataTable(){
    $this->current_rating_data_table = 'rating_data_'.date('Y').'_'.date('n');
    $this->current_rating_data_daily_devices = 'rating_data_daily_devices_'.date('Y').'_'.date('n');
}

private function getRegionNameByZipcode($data = array()){
    $zipcode = $data['zipcode'];

    $query = $this->db->select('zipcode.provinces_id,zipcode.zipcode,provinces.id,provinces.province_region')
    ->from('zipcode')
    ->join('provinces','provinces.id = zipcode.provinces_id')
    ->where('zipcode.zipcode',$zipcode)
    ->get();

    if($query->num_rows() > 0){
        return $query->row()->province_region;
    }else{
        return '';
    }

}

private function checkChennelExtendFrom($channels_id){

    $query = $this->db->select('id,channel_extended_status,channel_extended_from_channels_id')
    ->from('channels')
    ->where('id',$channels_id)
    ->where('channel_extended_status',1)
    ->where('channel_extended_from_channels_id !=',0)
    ->get();
    if($query->num_rows() > 0){
        return $query->row()->channel_extended_from_channels_id;
    }else{
        return $channels_id;
    }
}

private function checkTemporaryChennelExtendFrom($channels_id){

    $query = "select id,channel_extended_status,channel_extended_from_channels_id";
    $query .= " from channels";
    $query .= " where id = '".$channels_id."'";
    $query .= " and channel_extended_status = 1";
    $query .= " and channel_extended_from_channels_id != 0";

    $stmt = sqlsrv_query( $this->temporary_conn, $query,array(),array( "Scrollable" => 'static' )); 

    if(sqlsrv_num_rows($stmt) > 0){
        $row = sqlsrv_fetch_object($stmt);

        return $row->channel_extended_from_channels_id;
    }else{
        return $channels_id;
    }



}

private function checkVersionRatingString($arrRating){

    $exZeroKey = explode(',[', $arrRating[0]);
    $ship_code = $exZeroKey[0];
    $data_value = substr($exZeroKey[1], 0,-1);

    $exDatavalue = explode(',', $data_value);

        // print_r($exDatavalue);exit;

    if(in_array($exDatavalue[0], ['1','2','3'])){
        return 'new_version';
    }else{
        return 'old_version';
    }

}

private function setRatingOlderVersion($data = []){

    $awesomeArray = $data['awesomeArray'];
    $arrRating = $data['arrRating'];
    $ip_address = $data['ip_address'];

    foreach ($arrRating as $key => $value) {
                    # code...
        $data_value = "";
        if($key == 0){
            $exZeroKey = explode(',[', $value);
            $ship_code = $exZeroKey[0];
            $data_value = substr($exZeroKey[1], 0,-1);
            $awesomeArray['ship_code'] = $ship_code;
        }else{
            $sub_value = substr($value, 1);
            $data_value = substr($sub_value, 0,-1);

        }


        $data_value = $this->getDataValue($data_value);
        array_push($awesomeArray['records'], $data_value);
    }



    /* check devices data and zipcode data */
    $devices_id = $this->checkCreateDeviceRecord($awesomeArray);
    /* eof check device data and zipcode data */


    /* insert into rating data */
    foreach ($awesomeArray['records'] as $key => $value) {
                    # code...
        $channels_id = $this->getChannelsId($value);

        $channels_id = ($channels_id != '0')?$this->checkChennelExtendFrom($channels_id):$channels_id;

                    // comment for peak version


        $this->db->insert($this->current_rating_data_table,array(
            'devices_id'=>$devices_id,
            'channels_id'=>$channels_id,
            'ship_code'=>$awesomeArray['ship_code'],
            'frq'=>(int)$value['frq'],
            'sym'=>(int)$value['sym'],
            'pol'=>$value['pol'],
            'server_id'=>(int)$value['server_id'],
            'vdo_pid'=>(int)$value['vdo_pid'],
            'ado_pid'=>(int)$value['ado_pid'],
            'view_seconds'=>$value['view_seconds'],
            'startview_datetime'=>$value['created'],
            'created'=>date('Y-m-d H:i:s'),
            'ip_address'=>$ip_address
        )); 

                    // 


        /* set rating data daily */
        if($channels_id){
            $this->setRatingDataDaily(array(
                'date'=>date('Y-m-d',strtotime($value['created'])),
                'devices_id'=>$devices_id,
                'channels_id'=>$channels_id,
                'view_seconds'=>$value['view_seconds'],
                'ship_code'=>$awesomeArray['ship_code']
            ));
        }
        /* eof set rating data daily */
    }

}

private function setRatingNewVersion($data = []){

    $awesomeArray = $data['awesomeArray'];
    $arrRating = $data['arrRating'];
    $ip_address = $data['ip_address'];


        //print_r($awesomeArray);exit;
    unset($awesomeArray['records']);
    $awesomeArray['Satellite'] = [];
    $awesomeArray['IPTV'] = [];
    $awesomeArray['Youtube'] = [];


        // print_r($awesomeArray);exit;

    foreach ($arrRating as $key => $value) {
            // print_r($value);

        $data_value = "";
        if($key == 0){
            $exZeroKey = explode(',[', $value);
            $ship_code = $exZeroKey[0];
            $data_value = substr($exZeroKey[1], 0,-1);
            $awesomeArray['ship_code'] = $ship_code;
        }else{
            $sub_value = substr($value, 1);
            $data_value = substr($sub_value, 0,-1);

        }

        $rating_type = $this->checkRatingTypeByDataValue($data_value);

            //print_r($rating_type);
        switch ($rating_type) {
            case 'Satellite':
            array_push($awesomeArray['Satellite'], $this->getSatelliteDataByDataValue($data_value));
            break;

            /* by pass for check bandwidth */

            case 'IPTV':
            array_push($awesomeArray['IPTV'], $this->getIPTVDataByDataValue([
                'data_value'=>$data_value,
                'chip_code'=>$awesomeArray['ship_code']
            ]));
            break;
            case 'Youtube':
            array_push($awesomeArray['Youtube'], $this->getYoutubeDataByDataValue($data_value));

            break;    

            /* eof by pass for check bandwidth */

            default:
                    # code...
            break;
        }
    }

        // print_r($awesomeArray);exit;

    $devices_id = $this->checkCreateDeviceRecord($awesomeArray);

        // print_r($devices_id);exit;
        //exit;

    if(count($awesomeArray['Satellite']) > 0){
        $this->setSatelliteRating([
            'arr_data'=>$awesomeArray['Satellite'],
            'devices_id'=>$devices_id,
            'chip_code'=>$awesomeArray['ship_code'],
            'ip_address'=>$awesomeArray['ip_address']
        ]);
    }

    /* by pass for check bandwidth */

    if(count($awesomeArray['IPTV']) > 0){
        $this->setIPTVRating([
            'arr_data'=>$awesomeArray['IPTV'],
            'devices_id'=>$devices_id,
            'chip_code'=>$awesomeArray['ship_code'],
            'ip_address'=>$awesomeArray['ip_address']
        ]);

    }

    if(count($awesomeArray['Youtube']) > 0){
        $this->setYoutubeRating([
            'arr_data'=>$awesomeArray['Youtube'],
            'devices_id'=>$devices_id,
            'chip_code'=>$awesomeArray['ship_code'],
            'ip_address'=>$awesomeArray['ip_address']
        ]);
    }

    /* eof by pass for check bandwidth */



}

private function checkRatingTypeByDataValue($data_value = ""){ 

    // print_r($data_value);exit;

    $this->load->config('api');
    $arr_rating_type = $this->config->item('arr_rating_type');

    $exDatavalue = explode(',', $data_value);


    $exDataValue = explode(',', $data_value);

    // print_r($exDatavalue);exit;

    return $arr_rating_type[$exDatavalue[0]];



}

private function getSatelliteDataByDataValue($data_value = ""){

    $exDatavalue = explode(',',$data_value);

        // print_r($exDatavalue);exit;
    return array(
        'frq'=>$exDatavalue[2],
        'sym'=>$exDatavalue[3],
        'pol'=>$exDatavalue[4],
        'server_id'=>$exDatavalue[5],
        'vdo_pid'=>$exDatavalue[6],
        'ado_pid'=>$exDatavalue[7],
        'view_seconds'=>$exDatavalue[9],
        'created'=>$this->getRatingDateFormat($exDatavalue[8]),
        'channel_ascii_code'=>(int)$exDatavalue[1]
    );

}

private function getIPTVDataByDataValue($data = []){

    $data_value = $data['data_value'];
    $chip_code = $data['chip_code'];

    $exDatavalue = explode(',', $data_value);
    $shorten_url  = explode('?', $exDatavalue[1], 2)[0];

        // print_r($exDatavalue);exit;

        //print_r($shorten_url);exit;

    return [
        'tvchannels_url'=>$shorten_url,
        'created'=>$this->getRatingDateFormat($exDatavalue[2]),
        'view_seconds'=>$exDatavalue[3],
        'chip_code'=>$chip_code

    ];
        //print_r($exDatavalue);exit;

}


private function getYoutubeDataByDataValue($data_value = ""){
    $exDatavalue = explode(',', $data_value);

        // print_r($exDatavalue);exit;
    return [
        'youtube_url'=>$exDatavalue[1],
        'created'=>$this->getRatingDateFormat($exDatavalue[2]),
        'view_seconds'=>$exDatavalue[3]
    ];

}


private function setSatelliteRating($data = []){

    $devices_id = $data['devices_id'];
    $chip_code = $data['chip_code'];
    $ip_address = $data['ip_address'];

    foreach ($data['arr_data'] as $key => $value) {

            //print_r($value);exit;
                    # code...
        $channels_id = $this->getChannelsId($value);

        $channels_id = ($channels_id != '0')?$this->checkChennelExtendFrom($channels_id):$channels_id;

                    // echo $channels_id;exit;

                    // comment for peak time 

        $this->db->insert($this->current_rating_data_table,array(
            'devices_id'=>$devices_id,
            'channels_id'=>$channels_id,
            'ship_code'=>$chip_code,
            'frq'=>(int)$value['frq'],
            'sym'=>(int)$value['sym'],
            'pol'=>$value['pol'],
            'server_id'=>(int)$value['server_id'],
            'vdo_pid'=>(int)$value['vdo_pid'],
            'ado_pid'=>(int)$value['ado_pid'],
            'view_seconds'=>$value['view_seconds'],
            'startview_datetime'=>$value['created'],
            'channel_ascii_code'=>$value['channel_ascii_code'],
            'created'=>date('Y-m-d H:i:s'),
            'ip_address'=>$ip_address
        )); 


                    // eof comment for peak time 


        /* set rating data daily */
        if($channels_id){
            $this->setRatingDataDaily(array(
                'date'=>date('Y-m-d',strtotime($value['created'])),
                'devices_id'=>$devices_id,
                'channels_id'=>$channels_id,
                'view_seconds'=>$value['view_seconds'],
                'ship_code'=>$chip_code
            ));
        }
        /* eof set rating data daily */

                    //exit;
    }





}

private function setTemporarySatelliteRating($data = []){

    $devices_id = $data['devices_id'];
    $chip_code = $data['chip_code'];
    $ip_address = $data['ip_address'];

    //print_r($this->current_rating_data_table);exit;

    foreach ($data['arr_data'] as $key => $value) {

            //print_r($value);exit;
                    # code...
        // $channels_id = $this->getChannelsId($value);
        $channels_id = $this->getTemporaryChannelsId($value);

        $channels_id = ($channels_id != '0')?$this->checkTemporaryChennelExtendFrom($channels_id):$channels_id;

                    // echo $channels_id;exit;

                    // comment for peak time 





        $query_insert_rating_data_table = "insert into ".$this->current_rating_data_table."(
            devices_id,channels_id,ship_code,frq,sym,pol,server_id,vdo_pid,ado_pid,view_seconds,startview_datetime,channel_ascii_code,created,ip_address
        ) values (
            '".$devices_id."',
            '".$channels_id."',
            '".$chip_code."',
            '".(int)$value['frq']."',
            '".(int)$value['sym']."',
            '".$value['pol']."',
            '".(int)$value['server_id']."',
            '".(int)$value['vdo_pid']."',
            '".(int)$value['ado_pid']."',
            '".$value['view_seconds']."',
            '".$value['created']."',
            '".$value['channel_ascii_code']."',
            '".date('Y-m-d H:i:s')."',
            '".$ip_address."'
        )";

        sqlsrv_query($this->temporary_conn,$query_insert_rating_data_table,array(),array(
            "Scrollable"=>'static'
        )); 



        // $this->db->insert($this->current_rating_data_table,array(
        //     'devices_id'=>$devices_id,
        //     'channels_id'=>$channels_id,
        //     'ship_code'=>$chip_code,
        //     'frq'=>(int)$value['frq'],
        //     'sym'=>(int)$value['sym'],
        //     'pol'=>$value['pol'],
        //     'server_id'=>(int)$value['server_id'],
        //     'vdo_pid'=>(int)$value['vdo_pid'],
        //     'ado_pid'=>(int)$value['ado_pid'],
        //     'view_seconds'=>$value['view_seconds'],
        //     'startview_datetime'=>$value['created'],
        //     'channel_ascii_code'=>$value['channel_ascii_code'],
        //     'created'=>date('Y-m-d H:i:s'),
        //     'ip_address'=>$ip_address
        // )); 


                    // eof comment for peak time 


        /* set rating data daily */
        if($channels_id){

            $this->setTemporaryRatingDataDaily([
                'date'=>date('Y-m-d',strtotime($value['created'])),
                'devices_id'=>$devices_id,
                'channels_id'=>$channels_id,
                'view_seconds'=>$value['view_seconds'],
                'ship_code'=>$chip_code
            ]);

            // $this->setRatingDataDaily(array(
            //     'date'=>date('Y-m-d',strtotime($value['created'])),
            //     'devices_id'=>$devices_id,
            //     'channels_id'=>$channels_id,
            //     'view_seconds'=>$value['view_seconds'],
            //     'ship_code'=>$chip_code
            // ));
        }
        /* eof set rating data daily */

                    //exit;
    }





}

private function setIPTVRating($data = []){


    $chip_code = $data['chip_code'];
    $ip_address = $data['ip_address'];


    /* curl to s3remote api and then make rating data by s3remote controller */
    foreach ($data['arr_data'] as $key => $value) {
            # code...
        $value['ip_address'] = $ip_address;
        $value['chip_code'] = $chip_code;
            //print_r($value);exit;
        $response = $this->setIPTVRatingToS3RemoteServer($value);
    }

        //exit;
    /* eof curl s3remote  */



}

private function setTemporaryIPTVRating($data = []){

     $chip_code = $data['chip_code'];
    $ip_address = $data['ip_address'];


    /* curl to s3remote api and then make rating data by s3remote controller */
    foreach ($data['arr_data'] as $key => $value) {
            # code...
        $value['ip_address'] = $ip_address;
        $value['chip_code'] = $chip_code;
            //print_r($value);exit;
        // $response = $this->setIPTVRatingToS3RemoteServer($value);
        $response = $this->setTemporaryIPTVRatingToS3RemoteServer($value);
    }

        //exit;
    /* eof curl s3remote  */




    
}

private function setYoutubeRating($data = []){

    $youtube_log_table = 'youtube_rating_log_'.date('Y').'_'.date('n');

    /* check already create current log table  */
    $this->checkAlreadyCreateYoutubeLogsTable([
        'table_name'=>$youtube_log_table
    ]);
    /* eof check already create current log table  */

        // print_r($data);exit;
    $chip_code = $data['chip_code'];
    $ip_address = $data['ip_address'];

    /* check this devices already create youtube rating daily devices  */
    $queryCheck = $this->db->select('id,date,devices_id,times')
    ->from('youtube_rating_daily_devices')
    ->where('date',date('Y-m-d'))
    ->where('devices_id',$data['devices_id'])
    ->get();


    if($queryCheck->num_rows() <= 0){ /* not found this record and this date before */

        $this->db->insert('youtube_rating_daily_devices',[
            'date'=>date('Y-m-d'),
            'devices_id'=>$data['devices_id'],
            'times'=>1,
            'created'=>date('Y-m-d H:i:s'),
            'updated'=>date('Y-m-d H:i:s')
        ]);

    }else{
        $row_check = $queryCheck->row();
        $this->db->update('youtube_rating_daily_devices',[
            'times'=>$row_check->times+count($data['arr_data']),
            'updated'=>date('Y-m-d H:i:s')
        ],['id'=>$row_check->id]);

    }

    foreach ($data['arr_data'] as $key => $value) {
            # code...
        /* insert data into  log table */

                // comment for peak time 
        $this->db->insert($youtube_log_table,[
            'devices_id'=>$data['devices_id'],
            'chip_code'=>$data['chip_code'],
            'startview_datetime'=>$value['created'],
            'video_url'=>$value['youtube_url'],
            'view_seconds'=>(int)$value['view_seconds'],
            'created'=>date('Y-m-d H:i:s')
        ]);

                // eof comment for peak

        /* eof check */
    }




}

private function setIPTVRatingToS3RemoteServer($data = []){

    $this->load->config('api');

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$this->config->item('s3remote_iptv_rating_url'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$data);

        // In real life you should use something like:
        // curl_setopt($ch, CURLOPT_POSTFIELDS, 
        //          http_build_query(array('postvar1' => 'value1')));

        // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close ($ch);

    return $server_output;
}

private function checkAlreadyCreateYoutubeLogsTable($data = []){
    $query = $this->db->query("select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME ='".$data['table_name']."'");

    if($query->num_rows() <= 0){
        /* create table for rating data */
        $strQuery = "CREATE TABLE ".$data['table_name']." (
        id int IDENTITY(1,1) PRIMARY KEY,
        devices_id int,
        chip_code varchar(255),
        startview_datetime datetime,
        video_url varchar(255),                    
        view_seconds int,
        created datetime
    )";

    $this->db->query($strQuery);

}
}

private function getDeviceChipCodeById($devices_id){
    $query = $this->db->select('id,ship_code')
    ->from('devices')
    ->where('id',$devices_id)
    ->get();

    if($query->num_rows() > 0){
        return $query->row()->ship_code;
    }else{
        return '';
    }
}

private function connectRatingSQLSRV2017(){

    $config = $this->sqlsrvConfig();

    if(ENVIRONMENT == 'development'){


        $serverName = $config['development']['servername']; //serverName\instanceName
        $connectionInfo = array( "Database"=>$config['db_name'],"CharacterSet" => "UTF-8");
        $this->sqlsrv_conn = sqlsrv_connect( $serverName, $connectionInfo);


        if(!$this->sqlsrv_conn){
           echo "Connection could not be established.<br />";
           die( print_r( sqlsrv_errors(), true));
       }
   }else if(ENVIRONMENT == 'testing'){
            $serverName = $config['testing']['servername']; //serverName\instanceName
            $connectionInfo = array( "Database"=>$config['db_name'], "UID"=>$config['testing']['username'], "PWD"=>$config['testing']['password'],"CharacterSet" => "UTF-8");

            $this->sqlsrv_conn = sqlsrv_connect( $serverName, $connectionInfo);

            
            if(!$this->sqlsrv_conn){
               echo "Connection could not be established.<br />";
               die( print_r( sqlsrv_errors(), true));
           }

       }else if(ENVIRONMENT == 'production'){

            $serverName = $config['production']['servername']; //serverName\instanceName
            $connectionInfo = array( "Database"=>$config['db_name'], "UID"=>$config['testing']['username'], "PWD"=>$config['testing']['password'],"CharacterSet" => "UTF-8");
            $this->sqlsrv_conn = sqlsrv_connect( $serverName, $connectionInfo);

            
            if(!$this->sqlsrv_conn){
               echo "Connection could not be established.<br />";
               die( print_r( sqlsrv_errors(), true));
           }



       }
   }

   private function sqlsrvConfig(){

    # SQL SERVER ( TEMP DB )
    $config['rating_sqlsrv_2017'] = array(
        'db_name' => 'S3Rating',
        'development' => array(
            'servername' => 'KRIDSADA-DELL\SQLEXPRESS',
            'username' => '',
            'password' => '',
        ),
        'testing' => array(
            'servername' => '192.168.100.23',
            'username' => 'Dev',
            'password' => 'Psi@dev',
        ),
        'production' => array(
            'servername' => '192.168.100.23',
            'username' => 'Dev',
            'password' => 'Psi@dev',

        )
    );

    return $config;

}

private function connectS3WebserverMYSQLRating(){

    $rating_mysql_connect = $this->config->item('rating_mysql_connect');

        //print_r($rating_mysql_connect);exit; 
    $connection_setting = $rating_mysql_connect[ENVIRONMENT];

        //print_r($connection_setting);exit;

        // Create connection
    $this->conn_mysql = new mysqli($connection_setting['servername'], $connection_setting['username'], $connection_setting['password'], $connection_setting['dbname']);
    $this->conn_mysql->set_charset("utf8");
        // Check connection
    if ($this->conn_mysql->connect_error) {
        die("Connection failed: " . $this->conn->connect_error);
    } 

}

private function insertDeviceToMYSLQ($data = []){
    $queryInsert = "insert into devices(
    ref_id,
    chipcode,
    created
    )values(
    '".$data['ref_id']."',
    '".$data['chipcode']."',
    '".date('Y-m-d H:i:s')."'
)";

$this->conn_mysql->query($queryInsert);
}

private function setRatingTemporary($data = []){

    // connect to sql server 192.168.100.23
    //$this->connectTemporaryDB();

    // print_r($this->temporary_conn);exit;



    $awesomeArray = $data['awesomeArray'];
    $arrRating = $data['arrRating'];
    $ip_address = $data['ip_address'];


        //print_r($awesomeArray);exit;
    unset($awesomeArray['records']);
    $awesomeArray['Satellite'] = [];
    $awesomeArray['IPTV'] = [];
    $awesomeArray['Youtube'] = [];


        // print_r($awesomeArray);exit;

    foreach ($arrRating as $key => $value) {
            // print_r($value);

        $data_value = "";
        if($key == 0){
            $exZeroKey = explode(',[', $value);
            $ship_code = $exZeroKey[0];
            $data_value = substr($exZeroKey[1], 0,-1);
            $awesomeArray['ship_code'] = $ship_code;
        }else{
            $sub_value = substr($value, 1);
            $data_value = substr($sub_value, 0,-1);

        }

            // print_r($data_value);
    }

    // print_r($data_value);exit;

    $rating_type = $this->checkRatingTypeByDataValue($data_value);



        //print_r($rating_type);
    switch ($rating_type) {
        case 'Satellite':
        array_push($awesomeArray['Satellite'], $this->getSatelliteDataByDataValue($data_value));
        break;

        /* by pass for check bandwidth */

        case 'IPTV':
        array_push($awesomeArray['IPTV'], $this->getIPTVDataByDataValue([
            'data_value'=>$data_value,
            'chip_code'=>$awesomeArray['ship_code']
        ]));
        break;
        case 'Youtube':
        array_push($awesomeArray['Youtube'], $this->getYoutubeDataByDataValue($data_value));

        break;    

        /* eof by pass for check bandwidth */

        default:
                    # code...
        break;
    }

    $devices_id = $this->checkCreateDeviceTemporaryRecord($awesomeArray);


    if(count($awesomeArray['Satellite']) > 0){
        $this->setTemporarySatelliteRating([
            'arr_data'=>$awesomeArray['Satellite'],
            'devices_id'=>$devices_id,
            'chip_code'=>$awesomeArray['ship_code'],
            'ip_address'=>$awesomeArray['ip_address']
        ]);
    }

    /* by pass for check bandwidth */

    if(count($awesomeArray['IPTV']) > 0){
        // $this->setTemporaryIPTVRating([
        //     'arr_data'=>$awesomeArray['IPTV'],
        //     'devices_id'=>$devices_id,
        //     'chip_code'=>$awesomeArray['ship_code'],
        //     'ip_address'=>$awesomeArray['ip_address']
        // ]);

    }

    if(count($awesomeArray['Youtube']) > 0){
        // $this->setTemporaryYoutubeRating([
        //     'arr_data'=>$awesomeArray['Youtube'],
        //     'devices_id'=>$devices_id,
        //     'chip_code'=>$awesomeArray['ship_code'],
        //     'ip_address'=>$awesomeArray['ip_address']
        // ]);
    }

    /* eof by pass for check bandwidth */



    // $devices_id = $this->checkCreateDeviceRecord($awesomeArray);


}

private function checkCreateDeviceTemporaryRecord($arrData = []){
    // $queryCheck = $this->db->select('id,ship_code')
    // ->from('devices')
    // ->where('ship_code',$arrData['ship_code'])
    // ->get();

    //print_r($arrData);exit;

    $query = "select id,ship_code from devices where ship_code = '".$arrData['ship_code']."'";

    // echo $query;exit;

        $stmt = sqlsrv_query( $this->temporary_conn, $query,array(),array( "Scrollable" => 'static' ));
        // echo ENVIRONMENT;exit;
        if(sqlsrv_num_rows($stmt) <= 0){
            // $result = sqlsrv_fetch_object($stmt);
            //print_r($result);exit;
            // insert new record 
            $insertDeviceQuery = "insert into devices(ship_code,ip_address,created) values (
                '".$arrData['ship_code']."',
                '".$arrData['ip_address']."',
                '".date('Y-m-d H:i:s')."'
            ); SELECT SCOPE_IDENTITY()";

            $resource= sqlsrv_query( $this->temporary_conn, $insertDeviceQuery,array(),array( "Scrollable" => 'static' ));
            sqlsrv_next_result($resource); 
            sqlsrv_fetch($resource);

            $insert_id = sqlsrv_get_field($resource, 0);

            if($insert_id){
                $this->load->config('api');

                $arr_province_region = $this->config->item('get_province_region_by_region_name');

                $province_region = "";

                $address = $this->getAddressByIpAddress(array(
                    'ip_address'=>$arrData['ip_address']
                ));

                if($address->region_name){
                    $key = array_search($address->region_name, array_column($arr_province_region, 'region_name'));

                    if(is_numeric($key)){
                        $province_region = $arr_province_region[$key]['province_region'];
                    }
                }

                // insert devices address query 
                $sqlInsertDeviceAddress = "insert into device_addresses(
                devices_id,continent_code,continent_name,country_code,country_name,region_code,region_name,province_region,city,zip,latitude,longitude,location,created
                ) values (
                    '".$insert_id."',
                    '".$address->continent_code."',
                    '".$address->continent_name."',
                    '".$address->country_code."',
                    '".$address->country_name."',
                    '".$address->region_code."',
                    '".$address->region_name."',
                    '".$province_region."',
                    '".$address->city."',
                    '".$address->zip."',
                    '".$address->latitude."',
                    '".$address->longitude."',
                    '".json_encode($address->location)."',
                    '".date('Y-m-d H:i:s')."'
                )";

                sqlsrv_query( $this->temporary_conn, $sqlInsertDeviceAddress,array(),array( "Scrollable" => 'static' ));
            }

            return $insert_id;




        }else{
            $result = sqlsrv_fetch_object($stmt);

            return $result->id;
            // print_r($result);exit;

        }





}


private function connectTemporaryDB(){

    #SQL SERVER ( TEMP DB)
    $config = [
        'servername'=>'192.168.100.23',
        'db_name'=>'S3Rating',
        'username'=>'Dev',
        'password'=>'Psi@dev'
    ];

    $serverName = $config['servername']; //serverName\instanceName
    $connectionInfo = array(
        "Database"=>$config['db_name'],
        "UID"=>$config['username'],
        "PWD"=>$config['password'],
        "CharacterSet" => "UTF-8"
    );

    $this->temporary_conn = sqlsrv_connect( $serverName, $connectionInfo);

            
    if(!$this->temporary_conn){
                 echo "Connection could not be established.<br />";
                 die( print_r( sqlsrv_errors(), true));
    }
}


}
