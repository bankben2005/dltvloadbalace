<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['ipstack'] = array(
	'api_key'=>array(
		'8a828c62a0bff809b2fb5fb54ac6a6ec',
		'6f1a01edf5301b0b94057e9ff0d09beb'
	)
);

$config['manual_range_list'] = array(
	'today'=>'วันนี้',
	'yesterday'=>'เมื่อวาน',
	'last_seven_days'=>'7 วันล่าสุด',
	'last_thirty_days'=>'30 วันล่าสุด',
	'this_month'=>'เดือนนี้',
	'last_month'=>'เดือนที่แล้ว',
	'custom'=>'กำหนดเอง'
);

$config['arr_twenty_four_hours'] = array(
	array(
		'start_time'=>'00:00',
		'end_time'=>'00:59'
	),
	array(
		'start_time'=>'01:00',
		'end_time'=>'01:59'
	),
	array(
		'start_time'=>'02:00',
		'end_time'=>'02:59'
	),
	array(
		'start_time'=>'03:00',
		'end_time'=>'03:59'
	),
	array(
		'start_time'=>'04:00',
		'end_time'=>'04:59'
	),
	array(
		'start_time'=>'05:00',
		'end_time'=>'05:59'
	),
	array(
		'start_time'=>'06:00',
		'end_time'=>'06:59'
	),
	array(
		'start_time'=>'07:00',
		'end_time'=>'07:59'
	),
	array(
		'start_time'=>'08:00',
		'end_time'=>'08:59'
	),
	array(
		'start_time'=>'09:00',
		'end_time'=>'09:59'
	),
	array(
		'start_time'=>'10:00',
		'end_time'=>'10:59'
	),
	array(
		'start_time'=>'11:00',
		'end_time'=>'11:59'
	),
	array(
		'start_time'=>'12:00',
		'end_time'=>'12:59'
	),
	array(
		'start_time'=>'13:00',
		'end_time'=>'13:59'
	),
	array(
		'start_time'=>'14:00',
		'end_time'=>'14:59'
	),
	array(
		'start_time'=>'15:00',
		'end_time'=>'15:59'
	),
	array(
		'start_time'=>'16:00',
		'end_time'=>'16:59'
	),
	array(
		'start_time'=>'17:00',
		'end_time'=>'17:59'
	),
	array(
		'start_time'=>'18:00',
		'end_time'=>'18:59'
	),
	array(
		'start_time'=>'19:00',
		'end_time'=>'19:59'
	),
	array(
		'start_time'=>'20:00',
		'end_time'=>'20:59'
	),
	array(
		'start_time'=>'21:00',
		'end_time'=>'21:59'
	),
	array(
		'start_time'=>'22:00',
		'end_time'=>'22:59'
	),
	array(
		'start_time'=>'23:00',
		'end_time'=>'23:59'
	)
); 	

$config['channel_countview_minus_time'] = "PT30M";

$config['get_province_region_by_region_name'] = [
	[
		'region_name'=>'Bangkok',
		'province_region'=>'Bangkok'
	],
	[
		'region_name'=>'Changwat Amnat Charoen',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Ang Thong',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Bueng Kan',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Buri Ram',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Chachoengsao',
		'province_region'=>'Eastern'
	],
	[
		'region_name'=>'Changwat Chai Nat',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Chaiyaphum',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Chanthaburi',
		'province_region'=>'Eastern'
	],
	[
		'region_name'=>'Changwat Chiang Rai',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Chon Buri',
		'province_region'=>'Eastern'
	],
	[
		'region_name'=>'Changwat Chumphon',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Kalasin',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Kamphaeng Phet',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Kanchanaburi',
		'province_region'=>'Western'
	],
	[
		'region_name'=>'Changwat Khon Kaen',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Krabi',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Lampang',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Lamphun',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Loei',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Lop Buri',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Mae Hong Son',
		'province_region'=>''
	],
	[
		'region_name'=>'Changwat Maha Sarakham',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Mukdahan',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Nakhon Nayok',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Nakhon Pathom',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Nakhon Phanom',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Nakhon Ratchasima',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Nakhon Sawan',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Nakhon Si Thammarat',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Nan',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Narathiwat',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Nong Bua Lamphu',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Nong Khai',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Nonthaburi',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Pathum Thani',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Pattani',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Phangnga',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Phatthalung',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Phayao',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Phetchabun',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Phetchaburi',
		'province_region'=>'Western'
	],
	[
		'region_name'=>'Changwat Phichit',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Phitsanulok',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Phra Nakhon Si Ayutthaya',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Phrae',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Prachin Buri',
		'province_region'=>'Eastern'
	],
	[
		'region_name'=>'Changwat Prachuap Khiri Khan',
		'province_region'=>'Western'
	],
	[
		'region_name'=>'Changwat Ranong',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Ratchaburi',
		'province_region'=>'Western'
	],
	[
		'region_name'=>'Changwat Rayong',
		'province_region'=>'Eastern'
	],
	[
		'region_name'=>'Changwat Roi Et',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Sa Kaeo',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Sakon Nakhon',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Samut Prakan',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Samut Sakhon',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Samut Songkhram',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Saraburi',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Satun',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Si Sa Ket',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Sing Buri',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Songkhla',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Sukhothai',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Suphan Buri',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Surat Thani',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Surin',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Tak',
		'province_region'=>'Western'
	],
	[
		'region_name'=>'Changwat Trang',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Trat',
		'province_region'=>'Eastern'
	],
	[
		'region_name'=>'Changwat Ubon Ratchathani',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Udon Thani',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Changwat Uthai Thani',
		'province_region'=>'Central'
	],
	[
		'region_name'=>'Changwat Uttaradit',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Changwat Yala',
		'province_region'=>'Southern'
	],
	[
		'region_name'=>'Changwat Yasothon',
		'province_region'=>'NorthEastern'
	],
	[
		'region_name'=>'Chiang Mai Province',
		'province_region'=>'Northern'
	],
	[
		'region_name'=>'Phuket',
		'province_region'=>'Southern'
	]

];


$config["arr_rating_type"] = [
	'1'=>'Satellite',
	'2'=>'IPTV',
	'3'=>'Youtube'
];


switch (ENVIRONMENT) {
	case 'development':
		$config["s3remote_iptv_rating_url"]  = "http://s3remoteservice.development/welcome/setIPTVRating";
	break;
	case 'testing':
		$config["s3remote_iptv_rating_url"]  = "http://s3remoteservice.psi.co.th/welcome/setIPTVRating";
	break;
	case 'production':
		$config["s3remote_iptv_rating_url"]  = "http://s3remoteservice.psi.co.th/welcome/setIPTVRating";
	break;
	
	default:
		$config["s3remote_iptv_rating_url"]  = "http://s3remoteservice.psi.co.th/welcome/setIPTVRating";
	break;
}

